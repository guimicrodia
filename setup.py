#!/usr/bin/env python
from distutils.core import setup

APP_NAME = 'guimicrodia'
APP_VERSION = '1.01'

setup(
	name=APP_NAME,
	version=APP_VERSION,
	description='a front-end to control the image quality of a V4L2 USB2 webcam',
	author='Neekhil',
	author_email='Nickel62Metal@Gmail.com',
	url='http://groups.google.com/group/microdia/',
	license='GNU GPL v3',

	packages=['guimicrodia'],
	package_dir={'guimicrodia': 'src/'},
	package_data={'guimicrodia': ['src/*']},


	data_files=[
			('share/applications', ['guimicrodia.desktop']),
			('share/pyshared/guimicrodia/', ['src/MicrodiaV4.glade', 'src/MicrodiaV4.xml', 'src/MicrodiaV4Header.png']),
			('share/doc/guimicrodia',['README', 'COPYING']),
		],
	scripts=['guimicrodia']

)

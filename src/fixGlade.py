#!/usr/bin/env python
# a quick script to fix Glade v3.4.0 XML errors
# - removes an unnecessary  <property name="response_id">0</property> line / string
# - Changes to comply with gtk-builder format
# the workflow is Glade -> fix_errata -> convert to GTK-Builder XML
#
# You must have gtk-builder-convert installed on your system
#
# Copyright 2008 Neekhil <Nickel62Metal@Gmail.com>
#   
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License with
# the Debian GNU/Linux distribution in file /usr/share/common-licenses/GPL-3;
# if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA  02111-1307  USA

__version__ = "1.00"
__author__ = "Neekhil <Nickel62Metal@Gmail.com>"
__date__ = "$ 28 Sep 2008 $"

import os
from os import system 
import sys

def determine_path ():
    """Borrowed from wxglade.py"""
    try:
        root = __file__
        if os.path.islink (root):
            root = os.path.realpath (root)
        return os.path.dirname (os.path.abspath (root))
    except:
        print "I'm sorry, but something is wrong."
        print "There is no __file__ variable. Please contact the author."
        sys.exit ()

original = open(determine_path () +"/MicrodiaV4.glade","r")
fixed = open(determine_path () +"/MicrodiaV4-fixed.glade",'w')
to_be_deleted = "<property name=\"response_id\">0</property>"

lines = original.readlines()

for line in lines:
	if line.find(to_be_deleted) == -1:
		fixed.write(line)
fixed.close()
original.close()

temp1 = determine_path () + "/MicrodiaV4-fixed.glade"
temp2 = determine_path () + "/MicrodiaV4.xml"

system("gtk-builder-convert "+temp1+" "+temp2)
system("rm "+temp1)

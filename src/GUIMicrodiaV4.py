#!/usr/bin/env python
# A Python app to manage Microdia V4L2 usb 2.0 webcam preferences/settings
# Currently implemented as a front-end to ivtv-utils instead of writing to v4l2 structs
#
# Copyright 2008 Neekhil <Nickel62Metal@Gmail.com>
#   
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License with
# the Debian GNU/Linux distribution in file /usr/share/common-licenses/GPL-3;
# if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA  02111-1307  USA

"A GUI for managing V4L2 Webcam Preferences"

__version__ = "1.01"
__author__ = "Neekhil <Nickel62Metal@Gmail.com>"
__date__ = "$ 06 Mar 2009 $"

# Have you made any recent changes to GUI via Glade ?, 
# If so, run script with this set to True atleast once
regenerate_gui_xml = False  

if(regenerate_gui_xml):
	import fixGlade #regenerate the GUI xml file

try:
	import sys
	import os
	from os import system
	import gtk
	import microdiaConfig
except:
	print "Error in importing"
	sys.exit(1)

def determine_path ():
    """Borrowed from wxglade.py"""
    try:
        root = __file__
        if os.path.islink (root):
            root = os.path.realpath (root)
        return os.path.dirname (os.path.abspath (root))
    except:
        print "I'm sorry, but something is wrong."
        print "There is no __file__ variable. Please contact the author."
        sys.exit ()

class guiMicrodia:
	brightness = 0.0
	contrast = 0.0
	saturation = 0.0
	gamma = 0.0
	exposure = 0.0
	sharpness = 0.0
	blue = 0.0
	red = 0.0
	hflip = False
	vflip = False
	nightmode = False
	reduceflicker = False

	def on_window_destroy(self, widget, data=None):
		gtk.main_quit()

	def __init__(self):
		builder = gtk.Builder()

		try:
			builder.add_from_file(determine_path () +"/MicrodiaV4.xml")
		except:
			print("Error: Did NOT find glade xml in current directory")

		self.window = builder.get_object("winMain")
		
		self.hsBrightness = builder.get_object("hsBrightness")
		self.hsContrast = builder.get_object("hsContrast")
		self.hsSaturation = builder.get_object("hsSaturation")
		self.hsGamma = builder.get_object("hsGamma")
		self.hsExposure = builder.get_object("hsExposure")
		self.hsSharpness = builder.get_object("hsSharpness")
		self.hsBlue = builder.get_object("hsBlue")
		self.hsRed = builder.get_object("hsRed")
		self.chkHflip = builder.get_object("chkHflip")
		self.chkVflip = builder.get_object("chkVflip")
		self.chkNightmode = builder.get_object("chkNightmode")
		self.chkReduceflicker = builder.get_object("chkReduceflicker")
		builder.connect_signals(self)
		#TODO: read config file into self.<>
		#TODO: apply all self.<> to v4l2-ctl
	def on_winMain_destroy(self, widget):
		sys.exit(0)
	def on_hsBrightness_value_changed(self, widget):
		a = widget.get_value()
		#TODO: convert input to a range that Brightness accepts, currently hardcoded
		self.brightness = (a/100)*0xff00
		print self.brightness
		brightnessWidget = widget
		system("v4l2-ctl --set-ctrl brightness="+str(self.brightness))
	def on_hsContrast_value_changed(self, widget):
		a = widget.get_value()
		print a
		self.contrast = (a/100)*0xff00
		print self.contrast
		system("v4l2-ctl --set-ctrl contrast="+str(self.contrast))
	def on_hsSaturation_value_changed(self, widget):
		a = widget.get_value()
		self.saturation = (a/100)*0xff00
		print a
	def on_hsGamma_value_changed(self, widget):
		a = widget.get_value()
		self.gamma = (a/100)*0xff00
		print a
	def on_hsExposure_value_changed(self, widget):
		a = widget.get_value()
		print a
		self.exposure = (a/100)*0xff00
		print self.exposure
		system("v4l2-ctl --set-ctrl exposure="+str(self.exposure))
	def on_hsSharpness_value_changed(self, widget):
		a = widget.get_value()
		print a
		self.sharpness = (a/100)*0xff00
		print self.sharpness
		system("v4l2-ctl --set-ctrl sharpness="+str(self.sharpness))
	def on_hsBlue_value_changed(self, widget):
		a = widget.get_value()
		print a
		self.blue = (a/100)*0x7f
		print self.blue
		system("v4l2-ctl --set-ctrl blue_balance="+str(self.blue))
	def on_hsRed_value_changed(self, widget):
		a = widget.get_value()
		#print a
		self.red = (a/100)*0x7f
		print self.red
		system("v4l2-ctl --set-ctrl red_balance="+str(self.red))
	def on_chkHflip_toggled(self, widget):
		self.hflip = widget.get_active()
		print type(self.hflip)
		print("v4l2-ctl --set-ctrl horizontal_flip="+str(self.hflip))
		system("v4l2-ctl --set-ctrl horizontal_flip="+str(self.hflip))
	def on_chkVflip_toggled(self, widget):
		self.vflip = widget.get_active()
		print("v4l2-ctl --set-ctrl vertical_flip="+str(self.vflip))
		system("v4l2-ctl --set-ctrl vertical_flip="+str(self.vflip))
	def on_chkNightmode_toggled(self, widget):
		self.nightmode = widget.get_active()
		print self.nightmode
	def on_chkReduceflicker_toggled(self, widget):
		self.reduceflicker = widget.get_active()
		print self.reduceflicker
	def on_btnDefaults_clicked(self, widget):
		print "todo.. Defaults functionality"
		self.brightness = microdiaConfig.config['brightness']
		self.contrast = microdiaConfig.config['contrast']
		self.saturation = microdiaConfig.config['saturation']
		self.gamma = microdiaConfig.config['gamma']
		self.exposure = microdiaConfig.config['exposure']
		self.sharpness = microdiaConfig.config['sharpness']
		self.blue = microdiaConfig.config['blue']
		self.red = microdiaConfig.config['red']
		self.hflip = microdiaConfig.config['hflip']
		self.vflip = microdiaConfig.config['vflip']
		self.nightmode = microdiaConfig.config['nightmode']
		self.reduceflicker = microdiaConfig.config['reduceflicker']
		
		a = (float(self.brightness)*100)/0xff00
		self.hsBrightness.set_value(a)
		
		a = (float(self.contrast)*100)/0xff00
		self.hsContrast.set_value(a)

		a = (float(self.saturation)*100)/0xff00
		self.hsSaturation.set_value(a)
		
		a = (float(self.gamma)*100)/0xff00
		self.hsGamma.set_value(a)
		
		a = (float(self.exposure)*100)/0xff00
		self.hsExposure.set_value(a)

		a = (float(self.sharpness)*100)/0xff00
		self.hsSharpness.set_value(a)

		a = (float(self.blue)*100)/0x7f
		self.hsBlue.set_value(a)

		a = (float(self.red)*100)/0x7f
		self.hsRed.set_value(a)

		if (self.hflip == "True"):
			self.chkHflip.set_active(True)
		else:
			self.chkHflip.set_active(False)

		if (self.vflip == "True"):
			self.chkVflip.set_active(True)
		else:
			self.chkVflip.set_active(False)

		if (self.nightmode == "True"):
			self.chkNightmode.set_active(True)
		else:
			self.chkNightmode.set_active(False)

		if (self.reduceflicker == "True"):
			self.chkReduceflicker.set_active(True)
		else:
			self.chkReduceflicker.set_active(False)

		#TODO: Defaults Functionality
		#TODO: read config file into self.<>
		#TODO: apply all self.<> to v4l2-ctl
	def on_btnSave_clicked(self, widget):
		print "todo.. Save functionality"
		microdiaConfig.config['brightness'] = self.brightness
		microdiaConfig.config['contrast'] = self.contrast
		microdiaConfig.config['saturation'] = self.saturation
		microdiaConfig.config['gamma'] = self.gamma
		microdiaConfig.config['exposure'] = self.exposure
		microdiaConfig.config['sharpness'] = self.sharpness
		microdiaConfig.config['blue'] = self.blue
		microdiaConfig.config['red'] = self.red
		microdiaConfig.config['hflip'] = self.hflip
		microdiaConfig.config['vflip'] = self.vflip
		microdiaConfig.config['nightmode'] = self.nightmode
		microdiaConfig.config['reduceflicker'] = self.reduceflicker
		microdiaConfig.config.write()
		#TODO: Save Functionality
		#TODO: save all self.<> to config file
	def on_btnOk_clicked(self, widget):
		sys.exit(0)

#if __name__ == "__main__":
preferences = guiMicrodia()
gtk.main()

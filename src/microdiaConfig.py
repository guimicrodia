#!/usr/bin/env python
#R/W to a config file to maintain Microdia Preferences/Settings for persistent user sessions
#The directory ~/.webcam must be pre-existing & writable by the current user
#
# Copyright 2008 Neekhil <Nickel62Metal@Gmail.com>
#   
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License with
# the Debian GNU/Linux distribution in file /usr/share/common-licenses/GPL-3;
# if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA  02111-1307  USA
 
"a module to read/write config files"
__version__ = "1.00"
__author__ = "Neekhil <Nickel62Metal@Gmail.com>"
__date__ = "$ 28 Sep 2008 $"

from configobj import ConfigObj
import os

#TODO: check before creating directories & files
os.system("mkdir ~/.webcam")
os.system("touch ~/.webcam/microdia.cfg")

configFilePath = os.path.expanduser("~/.webcam/microdia.cfg")
config = ConfigObj(configFilePath)

